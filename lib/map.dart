import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_osm_plugin/flutter_osm_plugin.dart';
import 'package:harz/util.dart';

class OSMPage extends StatefulWidget {
  const OSMPage({Key? key}) : super(key: key);

  @override
  State<OSMPage> createState() => _OSMPageState();
}

class _OSMPageState extends State<OSMPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        elevation: 0,
        title: pad(
          vertical: 30,
          child: headline(context, "Stempelkarte"),
        ),
      ),
      body: SafeArea(
        child: ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
          child: MapWidget(),
        ),
      ),
    );
  }
}

class MapWidget extends StatelessWidget {
  MapWidget({Key? key}) : super(key: key);

  Future<MapController> initController() async {
    final markerPoints = [
      GeoPoint(latitude: 51.8241661, longitude: 10.7078448),
      GeoPoint(latitude: 51.8241661, longitude: 10.7078448),
      GeoPoint(latitude: 51.8241661, longitude: 10.7078448),
    ];

    final mapController = MapController(
        initMapWithUserPosition: false, initPosition: markerPoints.first)
      ..init();

    Future.delayed(
      Duration(seconds: 2),
      () async {
        await Future.wait(markerPoints.map(mapController.addMarker));
      },
    );

    return mapController;
  }

  late final controller = initController();

  @override
  Widget build(BuildContext context) {
    return controller.viewOrLoader(
      (context, value, child) => OSMFlutter(
        controller: value,
        trackMyPosition: false,
        initZoom: 12,
        minZoomLevel: 8,
        maxZoomLevel: 14,
        stepZoom: 1.0,
        userLocationMarker: UserLocationMaker(
          personMarker: MarkerIcon(
            icon: Icon(
              Icons.location_history_rounded,
              color: Colors.red,
              size: 48,
            ),
          ),
          directionArrowMarker: MarkerIcon(
            icon: Icon(
              Icons.double_arrow,
              size: 48,
            ),
          ),
        ),
        roadConfiguration: RoadConfiguration(
          startIcon: MarkerIcon(
            icon: Icon(
              Icons.person,
              size: 64,
              color: Colors.brown,
            ),
          ),
          roadColor: Colors.yellowAccent,
        ),
        markerOption: MarkerOption(
            defaultMarker: MarkerIcon(
          icon: Icon(
            Icons.person_pin_circle,
            color: Colors.blue,
            size: 56,
          ),
        )),
      ),
    );
  }
}
