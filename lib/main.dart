import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:harz/map.dart';
import 'util.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        textTheme: GoogleFonts.ubuntuTextTheme(Theme.of(context).textTheme),
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

final accentWhite = Color.fromARGB(255, 247, 247, 247);

Widget _pad(Widget child) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 24),
    child: child,
  );
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: ListTile(
                  contentPadding: EdgeInsets.zero,
                  leading: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [Icon(Icons.person, size: 30)],
                  ),
                  title: const Text("Ludwig Wagener"),
                  subtitle: const Text("Level 13"),
                  trailing: const Icon(Icons.notifications_active_outlined),
                ),
              ),
              _pad(headline(context, "Stempel")),
              _pad(
                Material(
                  elevation: 20,
                  borderRadius: BorderRadius.circular(18)
                      .copyWith(topRight: const Radius.circular(44)),
                  color: accentWhite,
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          SizedBox(
                            width: 110,
                            height: 130,
                            child: Column(
                              children: [
                                pad(
                                    bottom: 8,
                                    child: Text("73 Stempel gesammelt",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2
                                            ?.copyWith(
                                                fontSize: 18,
                                                color: Colors.lightGreen))),
                                pad(
                                    bottom: 8,
                                    child: Text("42 Stempel offen",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2
                                            ?.copyWith(
                                                fontSize: 18,
                                                color: Colors.red))),
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(),
                                    borderRadius: BorderRadius.circular(16),
                                    color: Colors.transparent,
                                  ),
                                  child: InkWell(
                                    borderRadius: BorderRadius.circular(16),
                                    onTap: () {
                                      print("hello world");
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 4, horizontal: 9),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text("Zufälliger",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .button),
                                          const SizedBox(
                                            width: 3,
                                            height: 1,
                                          ),
                                          const Icon(Icons.approval_outlined,
                                              size: 18)
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SizedBox(
                                height: 130,
                                child: Image.network(
                                    "https://media.discordapp.net/attachments/701567821037174955/1030179491911311490/unknown.png?width=387&height=390"),
                              ),
                              Text("Zuletzt gestempelt",
                                  style: Theme.of(context).textTheme.caption),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              _pad(headline(context, "Abzeichen")),
              SizedBox(
                height: 120,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Row(
                    children: [
                      card(
                          "https://media.discordapp.net/attachments/701567821037174955/1030461462919000074/unknown.png?width=494&height=397"),
                      spacer,
                      card(
                          "https://media.discordapp.net/attachments/701567821037174955/1030461519558873088/unknown.png?width=526&height=385"),
                    ],
                  ),
                ),
              ),
              spacer,
              SizedBox(
                height: 120,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Row(
                    children: [
                      card(
                          "https://media.discordapp.net/attachments/701567821037174955/1030461549313273906/unknown.png?width=504&height=388"),
                      spacer,
                      card(
                          "https://media.discordapp.net/attachments/701567821037174955/1030461587787624458/unknown.png?width=404&height=400"),
                    ],
                  ),
                ),
              ),
              _pad(headline(context, "Karte")),
              _pad(
                Material(
                  elevation: 20,
                  borderRadius: BorderRadius.circular(18)
                      .copyWith(topRight: const Radius.circular(44)),
                  color: accentWhite,
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return const OSMPage();
                            },
                          ),
                        );
                      },
                      child: IgnorePointer(
                        ignoring : true,
                        child: SizedBox(
                          height: 400,
                          child: MapWidget(),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

final spacer = Container(width: 16, height: 16, color: Colors.transparent);

Widget card(String imageUrl) {
  return Expanded(
    child: Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18),
      ),
      elevation: 20,
      child: Center(
        child: pad(all: 8, child: Image.network(imageUrl)),
      ),
    ),
  );
}

class Section extends StatelessWidget {
  final Widget child;
  final String title;

  const Section({key, required this.child, required this.title})
      : super(key: null);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: [
          Text(title, style: Theme.of(context).textTheme.headline4),
          const Spacer(),
        ]),
        child,
      ],
    );
  }
}
