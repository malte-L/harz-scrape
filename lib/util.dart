import "package:flutter/material.dart";

Widget pad(
    {final double? all,
    final double? vertical,
    final double? horizontal,
    final double? bottom,
    required final Widget child}) {
  assert(
      all != null || vertical != null || horizontal != null || bottom != null,
      "pad(): all paddings are null");

  final padding = all != null
      ? EdgeInsets.all(all)
      : vertical != null || horizontal != null
          ? EdgeInsets.symmetric(
              vertical: vertical ?? 0.0, horizontal: horizontal ?? 0.0)
          : EdgeInsets.only(bottom: bottom ?? 0.0);


  return Padding(padding: padding, child: child);
}



Widget headline(BuildContext context, String text) {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 16),
    child: Text(
      text,
      style: Theme.of(context).textTheme.headline4!.copyWith(
          color: Color.fromARGB(255, 73, 73, 73), fontWeight: FontWeight.bold),
    ),
  );
}


extension ViewFuture<T> on Future<T> {
  Widget viewOrLoader(ValueWidgetBuilder builder) {
    return FutureBuilder(
      future: this,
      builder: (context, AsyncSnapshot<T> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        if (snapshot.hasError) {
          print(snapshot.error.toString());
          return Center(
            child: Text(snapshot.error.toString()),
          );
        }

        return builder(context, snapshot.data!, null);
      },
    );
  }
}
